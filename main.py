from flask import Flask
from app import app
from app.api.user_api import user_post,user_get,user_update,user_delete

if __name__ == '__main__':
    app.run(debug=True)
