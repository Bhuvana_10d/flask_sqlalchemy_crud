from app import db
from app.models.user_db import User
from flask import request

def user_post():
    id = request.form.get('id')
    name = request.form.get('name')
    address = request.form.get('address')
    user = User()
    user.id = id
    user.name = name
    user.address = address
    db.session.add(user)
    db.session.commit()
    return 'New user was inserted'

def user_get():
    id = request.form.get('id')
    fetchUser=User.query.filter(User.id==id).first()
    print(fetchUser.id)
    print(fetchUser.name)
    print(fetchUser.address)
    return 'The users you fetched'
    # name = request.form.get('name')
    # address = request.form.get('address')
    # user = User()
    # user.id = id
    # user.name=name
    # user.address=address
    # user = User.query.filter_by(id=id,name=name,address=address).all()
    # print(user.name)
    # print(user)
    # # db.session.add(user)
    # # db.session.commit()


def user_update():
    name = request.form.get('name')
    address = request.form.get('address')
    user = User.query.filter(User.name==name).first()
    user.name = name
    user.address=address
    db.session.commit()
    return 'The changes you have made was updated'

def user_delete():
    id = request.form.get('id')
    print(id)
    # name = request.form.get('name')
    # address = request.form.get('address')
    user = User.query.filter(User.id==id).delete()
    # user.name = name
    # user.address = address
    # db.session.delete(user)
    db.session.commit()
    return 'The user you want to remove was deleted'
