from app.services.users import user_post,user_get,user_update,user_delete
from app import app

@app.route('/add', methods=['POST'])
def added_users():
    return user_post()


@app.route("/get", methods=["GET"])
def get():
    return user_get()


@app.route("/update", methods=["PUT"])
def update():
    return user_update()


@app.route("/delete", methods=["DELETE"])
def delete():
    return user_delete()
